<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 04/07/17
 * Time: 15:15
 */

require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('modelsFolder').'areasConocimiento/AreasConocimiento.php';
require_once $config->get('utilsFolder').'Correos.php';


class ContactoController extends ControllerBase
{
    public function goContacto()
    {
        $data['bFlag'] = 1;

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('contacto.php', $data);

    }

    public function doContacto()
    {
        $aForm = $this->_request['form'];

        $oCorreo = new Correos();

        $oCorreo->enviarMail(array('contacto@phdtalks.com'), $this->_config->get('projectEmailFrom'), "Comentarios recibidos desde la página web", 'contacto.php', $aForm);

        $data['success'] = true;

        $this->_view->showJson($data);

    }


}