<?php
/**
 * Clase VideosController
 *
 * La clase manipula las acciones de los videos del sistema
 *
 * Creado 11/Abril/2017
 *
 * @category Class
 * @package Controllers
 * @author Emmanuel
 */

require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';

class VideosController extends ControllerBase {

    public function init()
    {

    }

    public function goVisualizar()
    {
        //Item activo del menú
        $data['activeItem'] = 'inicio';
        //Obtener datos del video a analizar
        $data['aInfoVideo'] = Videos::obtnerInfoVideo($this->_request['id']);
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();
        $this->_view->showMain('videos/visualizacion.php', $data);

    }

    public function goVideosArea()
    {
        $nIdArea = $this->_request['id'];
        //Item activo del menú
        $data['activeItem'] = 'inicio';
        //Obtener videos
        $data['aVideosAreas'] = Videos::obtenerVideosPorArea($nIdArea);

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        foreach($data['areasConocimiento'] as $aAreasConocimiento)
        {
            if($nIdArea == $aAreasConocimiento['id'])
            {
                $data['sAreaConocimiento'] = $aAreasConocimiento['nombre'];
            }
        }

        $this->_view->showMain('videos/videosPorArea.php', $data);

    }

    public function goBuscar()
    {
        //se obtiene las palabras "r" es la variable que pasa 'buscar?r=" '
        $sPalabra = $this->_request['r'];

        //Se obtienen las categorias para el menú
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        //Se obtienen los videos que coincidan con la palabra
        $data['busqueda'] = Videos::buscarVideo($sPalabra);
        //$data['busqueda']['areas'] = Videos::obtnerInfoVideo($data['busqueda']['videos']);
        $data['busqueda']['palabra'] = $sPalabra;

        $this->_view->showMain('videos/buscar.php', $data);

    }

}

