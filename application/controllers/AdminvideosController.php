<?php

require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';

class AdminvideosController extends ControllerBase{

    public function init()
    {
        Autentificar::validarLogin();

    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            'Nuevo video'
        );

        $aInvestigadores = Investigadores::obtenerInvestigadores();
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

        $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aNavegacion', 'aSubAreasConocimiento', 'aInvestigadores'));
    }

    public function doAgregar()
    {
        $aForm = $this->_request['form'];

        if(!$nIdInvestigador = $aForm['idInvestigador'])
        {
            $nIdInvestigador = Session::get('idInvestigador');
        }

        $oResponse = new ResponseForm($aForm);

        //Si el video ya existe se agrega un error
        if($aVideo = Videos::where(array("videos.`ON` = '1' AND (videos.titulo = '{$aForm['titulo']}' OR videos.liga_youtube = '{$aForm['liga_youtube']}')")))
        {
            $oResponse->addErrorMensaje('Error ya existe un video con el mismo título o liga de YouTube');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            //Item activo del menú
            $activeItem = 'inicio';
            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Agregar video"
            );


            $this->_view->showSistemaMain('admin/videos/formulario.php', compact('oErrors', 'activeItem', 'aNavegacion', 'aSubAreasConocimiento'));
            return;

        }

        //Se obtiene el ID del area del conocimiento
        $aSubArea = SubAreas::where(array("`ON` = 1 AND id = {$aForm['subareaConocimiento']}"), array('id_area_conocimiento'));

        $aVideo = array(
            'titulo' => $aForm['titulo'],
            'liga_youtube' => $aForm['liga_youtube'],
            'liga_articulo' => $aForm['liga_articulo'],
            'descripcion' => $aForm['descripcion'],
            'id_investigador' => $nIdInvestigador,
            'id_area_conocimiento' => $aSubArea[0]['id_area_conocimiento'],
            'id_subarea_conocimiento' => $aForm['subareaConocimiento'],
            'fecha_captura' => date('Y-m-d H:i:s'),
            'palabras_clave' => $aForm['palabras_clave'],
            'coautores' => $aForm['coautores'],
            'journal' => $aForm['journal'],
            'visible' => '1'
        );

        $nIdVideo = Videos::agregarVideo($aVideo);

        ResponseForm::addFlashNotice('Se ha agregado el video correctamente'); //agregar mensajes

        if((Session::get('idPerfil')) == '1'){
            $this->_redirect($this->_config->get('baseUrl') . 'admin/videos/listado');
            return;

        }

        $this->_redirect($this->_config->get('baseUrl') . 'investigador/index');

    }

    public function ajaxEliminarVideo()
    {
        $nIdVideo = $this->_request['video'];

        /*Se elimina el video*/
        $aResult = Videos::eliminarVideo($nIdVideo);

        $this->_view->showJson($aResult);

    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar videos'
        );

        $this->_view->showSistemaMain('admin/videos/listado.php', compact('aNavegacion'));

    }

    public function jsonVideos()
    {

        $aVideos = Videos::obtenerVideos(Session::get('idInvestigador'));

        $this->_view->showJson(array('data' => $aVideos));
    }

    public function goEditar()
    {
        $bActualizar = '1'; /*boleano*/
        $aVideo =  Videos::obtnerInfoVideo($this->_request['video']); /*manda los datos*/
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();
        $activeItem = 'adminVideos';


        //Barra de navegación
        if((Session::get('idPerfil')) == '1')
        {
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Actualizar video"
            );

        }else{
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Actualizar video"
            );
        }

        $this->_view->showSistemaMain('admin/videos/formulario.php', compact('bActualizar','aVideo','activeItem','aNavegacion','aSubAreasConocimiento' ));

    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = $this->_request['video'];
        $bActualizar = 1;

        $aForm['visible'] == '1' ? $aForm['visible'] = '0' : $aForm['visible'] = '1';

        if($nIdVideo = Videos::agregarVideo($aForm))
        {
            $aVideo = Videos::obtnerInfoVideo($nIdVideo);

            ResponseForm::addFlashNotice('Se ha editado el video correctamente');

            if((Session::get('idPerfil')) == '1'){
                $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aVideo', 'bActualizar'));
                return;
            }

            $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aVideo', 'bActualizar'));


        }
    }
}