<?php

require_once $config->get('modelsFolder') . 'usuarios/UsuUsuarios.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';


class AdminusuariosController extends ControllerBase{

    public function init()
    {
        Autentificar::validarLogin();

    }

    public function goIndex()
    {

        $aNavegacion = array(
            'Bienvenida'
        );

        $this->_view->showSistemaMain('admin/usuarios/index.php', compact('aNavegacion'));

    }

    public function goRegistrar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
            'Nuevo administrador'
        );

        $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('aNavegacion'));
    }

    public function doRegistrar()
    {
        $aForm = $this->_request['form'];
        $aForm['idPerfil'] = '1';

        $oResponse  = new ResponseForm();

        if(UsuUsuarios::where(array("usu_usuarios.email = '{$aForm['email']}' AND usu_usuarios.`ON` = 1")))
        {
            $oResponse->addErrorMensaje('Error ya existe un usuario registrado con el correo: ' . ' ' . $aForm['email']);

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('oErrors'));
            return;

        }

        if($oUsuario = UsuUsuarios::agregarUsuario($aForm))
        {
            ResponseForm::addFlashNotice('El usuario se registró correctamente con el correo: ' . $aForm['email'] );

            $this->_redirect($this->_config->get('baseUrl') . 'admin/usuarios/listado');

        }

    }

    public function ajaxEliminarUsuario()
    {
        $nIdUsuario = $this->_request['usuario'];

        $aResult = UsuUsuarios::eliminarUsuario($nIdUsuario);

        $this->_view->showJson($aResult);

    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar usuarios administradores'
        );

        $this->_view->showSistemaMain('admin/usuarios/listado.php', compact('aNavegacion'));
    }

    public function goMisDatos()
    {
        $bActualizar = '1';

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Mis datos'
        );

        $aUsuario = UsuUsuarios::obtenerUsuarioInfo(Session::get('idUsuario'));

        $this->_view->showSistemaMain('admin/usuarios/misDatos.php', compact('aNavegacion', 'aUsuario', 'bActualizar'));

    }

    public function doActualizarMisDatos()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = Session::get('idUsuario');

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Mis datos'
        );

        if(!UsuUsuarios::agregarUsuario($aForm))
        {
            $oResponse = new ResponseForm();

            $oResponse->addErrorMensaje('Se ha producido un error al guardar tus datos');

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/misDatos.php', compact('oErrors', 'aNavegacion'));
            return;

        }

        ResponseForm::addFlashNotice('Se han guardado tus datos correctamente');
        $this->_redirect($this->_config->get('baseUrl') . 'admin/misdatos');

    }

    public function goEditar()
    {
        $bActualizar = '1';

        $nIdUsuario = $this->_request['usuario'];

        $aUsuario = UsuUsuarios::obtenerUsuarioInfo($nIdUsuario);

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
            'Actualizar usuario administrador'
        );

        $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('aNavegacion', 'aUsuario', 'bActualizar'));

    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = $this->_request['usuario'];

        die(var_dump($aForm));


        if(!UsuUsuarios::agregarUsuario($aForm))
        {
            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
                'Actualizar usuario administrador'
            );

            $oResponse = new ResponseForm();

            $oResponse->addErrorMensaje('Se produjo un error al editar el usuario administrador');

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('oErrors', 'aNavegacion'));

            return;
        }

        ResponseForm::addFlashNotice('Se actualizó el usuario correctamente');

        $this->_redirect($this->_config->get('baseUrl') . 'admin/usuarios/listado');

    }

    public function jsonUsuarios()
    {
        $aUsuarios = UsuUsuarios::obtenerUsuarios(Session::get('idUsuario'));

        $this->_view->showJson(array('data' => $aUsuarios));

    }


}