<?php

class SubAreas extends ModelBase
{
    protected $sTable = 'subareas_conocimiento';
    protected $sPrimaryKey = 'id';

    public static function obtenerSubAreas()
    {
        $oModelo = new static();

        $sQuery = "
            SELECT subareas_conocimiento.id ,subareas_conocimiento.nombre AS subarea_nombre, areas_conocimiento.nombre AS area_nombre
            FROM subareas_conocimiento
            INNER JOIN areas_conocimiento ON areas_conocimiento.id = subareas_conocimiento.id_area_conocimiento
            WHERE subareas_conocimiento.`ON` = '1' AND areas_conocimiento.`ON` = '1'
        ";

        return $oModelo->_db->getAll($sQuery);
    }

/*    public static function obtenerIdAreaConocimiento*/

}