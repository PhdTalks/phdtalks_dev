<?php

class Investigadores extends ModelBase{

    protected $sTable = 'investigadores';
    protected $sPrimaryKey = 'id';

    public static function obtenerInvestigador($nIdInvestigador)
    {
        $oModelo = new static();

        $sQuery= "SELECT investigadores.id, investigadores.nombre, investigadores.apellido_p, investigadores.apellido_m, investigadores.fecha_nac, investigadores.sexo,investigadores.nacionalidad, investigadores.correo_institucional, investigadores.correo_personal, investigadores.numero_registro_cvu, investigadores.numero_registro_orcid,investigadores.institucion_academica, investigadores.pais_institucion, investigadores.nombramiento, investigadores.liga_cv, investigadores.liga_research_gate, usu_usuarios.`password`
                  FROM investigadores
                  INNER JOIN usu_usuarios
                  ON usu_usuarios.idInvestigador = investigadores.id
                  WHERE investigadores.`ON` = 1 AND investigadores.id = '{$nIdInvestigador}'";

        if(!$mInvestigadores = $oModelo->_db->getOne($sQuery))
        {
            return false;
        }
        return $mInvestigadores;

    }

    public static function eliminarInvestigador($nIdInvestigador)
    {
        if(!$nIdInvestigador)
        {
            return array('success' => false, 'msg' => 'No se especificó el investigador');
        }

        if(!$oInvestigador = self::findById($nIdInvestigador))
        {
            return array('success' => false, 'msg' => 'Investigador no encontrado en la base de datos');
        }

        if(!empty($oInvestigador))
        {
            $oInvestigador->ON = 0;
            $oInvestigador->save();
        }

        return array('success' => true);

    }

    public static function agregarInvestigador($aDatosInvestigador)
    {
        if($aDatosInvestigador['id'])
        {
            if(!$oInvestigador = Investigadores::findById($aDatosInvestigador['id']) )
            {
                return false;
            }

            $oInvestigador->update($aDatosInvestigador);

        }else{

            $oInvestigador = self::create($aDatosInvestigador);

        }

        return $oInvestigador->id;
    }

    public static function obtenerInvestigadores()
    {
        $oModelo = new static();

        $sQuery= "SELECT investigadores.id, CONCAT(investigadores.nombre,\" \", investigadores.apellido_p,\" \", investigadores.apellido_m) AS nombre, investigadores.fecha_nac, investigadores.sexo,investigadores.nacionalidad, investigadores.correo_institucional, investigadores.correo_personal, investigadores.numero_registro_cvu, investigadores.numero_registro_orcid,investigadores.institucion_academica, investigadores.pais_institucion, investigadores.nombramiento, investigadores.liga_cv, investigadores.liga_research_gate
                  FROM investigadores
                  WHERE investigadores.`ON` = 1";

        if(!$mInvestigadores = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }
        return $mInvestigadores;
    }

}