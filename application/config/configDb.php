<?php
$config = Config::getInstance();

$config->set('dbhost', 'localhost');
$config->set('dbname', 'phdtalks');
$config->set('dbuser', 'root');
$config->set('dbpass', 'root');
$config->set('baseDomain', 'http://phdtalks.local');
