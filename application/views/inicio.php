
<div class="nine wide column center buscadorVideos">
    <div class="ui fluid icon input buscadorSegundo">
        <input id="buscadorDown2" class="inputBuscador" placeholder="Busca por tema, investigador o disciplina" type="text">
        <i id="btnBuscadorDown2" class="btnBuscador search link icon"></i>
    </div>
</div>

<div id="contenido">
    <div class=" txtContPHD">
        <div class="reloj">
            <img src="assets/img/clock2.png">
        </div>
        <div>
            <p>  Estos son los videos más recientes:</p>
        </div>
    </div>

    <div id="videosRecientes" class="ui stackable three column grid container left aligned">
    <?foreach($aVideosRecientes as $aVideo){?>
        <div class="column">
            <div id="cardImg" class="ui card aligned center">
                <a class="image" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideo['id'])?>">
                    <img class="imagPhd" src="https://img.youtube.com/vi/<?echo($aVideo['imagen'])?>/mqdefault.jpg">
                </a>
                <div id="datosVideos" class="content left ">
                    <a class="header" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideo['id'])?>"><?echo($aVideo['titulo'])?></a>
                    <div class="description">
                         <p>Investigador: <span class="txtSubcribe"><?echo($aVideo['nombre']) . ' ' . $aVideo['apellido_p'] . ' ' . $aVideo['apellido_m']?></span><br>
                            Área del conocimiento: <span class="txtSubcribe"</span><?echo ($aVideo['areaConocimiento'])?></p>
                    </div>
                </div>
            </div>
        </div>
    <?}?>
    </div>
</div>
