<div id="contenido2">
    <?$aIdVideo = explode("=", $aInfoVideo['liga_youtube'])?>
    <div class="ui grid">
        <div class="row">
            <div class="column">
                <p class="txtTitulo">
                    <?echo($aInfoVideo['titulo'])?>
                </p>

                <div id="datosVideos">
                    <p class="txtSubcribe"><span class="colorTitulo">Investigador:</span> <?echo($aInfoVideo['nombre']) . ' ' . $aInfoVideo['apellido_p'] . ' ' . $aInfoVideo['apellido_m']?> <br>
                        <span class="colorTitulo">Disciplina:</span> <?echo($aInfoVideo['area'])?> > <?echo($aInfoVideo['subArea'])?><br>
                        <?if($aInfoVideo['journal']):?><span class="colorTitulo">Journal:</span> <?echo($aInfoVideo['journal'])?><br><?endif;?>
                        <?if($aInfoVideo['coautores']):?><span class="colorTitulo">Coautores:</span> <?echo($aInfoVideo['coautores'])?><br><?endif;?>

                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="eight wide column">
                <div class="video-responsive">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?echo($aIdVideo[1])?>" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                </div>
            </div>

            <div class="six wide column">
                <div id="datosVideos">
                    <p class="txtSubcribe"><span class="colorTitulo">Resumen:</span>
                        <p><?echo($aInfoVideo['descripcion']);?></p>

                </div>
                <?if($aInfoVideo['liga_articulo']){?>
                    <a href="<?echo($aInfoVideo['liga_articulo'])?>" target="_blank">
                        <div id="btnPHD">
                            <button id="buttonPHD" type="submit" class="ui basic button centered">
                                <p class="txtButton"> Ir al artículo</p>
                            </button>
                        </div>
                    </a>
                <?}?>
            </div>
        </div>
    </div>
</div>