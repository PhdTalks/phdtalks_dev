<div id="buscarVideos" class="section">
    <h2 class="ui header aligned centered tituloGeneral desktop-only">
        Buscar Videos
    </h2>
</div>
<div class="ui basic segment">
    <div id="mensajeBusqueda" class="ui message large">
        <p>Se encontraron <strong><?echo($busqueda['totalVideo'])?></strong> videos que coinciden con la búsqueda: <strong><?echo($busqueda['palabra'])?></strong></p>
    </div>

    <div id="listaVideosBuscar" class="ui stackable three column grid container left aligned">
        <? foreach($busqueda['videos'] as $aVideosEncontrados){?>
            <?$aIdVideo = explode("=", $aVideosEncontrados['liga_youtube'])?> <!--mostrar la imagen-->
            <div class="column">
                <div id="cardImg" class="ui card aligned center">
                    <a class="image" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideosEncontrados['id'])?>">
                        <img class="imagPhd" src="https://img.youtube.com/vi/<?echo($aIdVideo[1])?>/mqdefault.jpg">
                    </a>
                    <div id="datosVideos" class="content left ">
                        <a class="header" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideosEncontrados['id'])?>">“<?echo($aVideosEncontrados['titulo'])?>”</a>
                        <div class="description">
                            <p>Investigador: <span class="txtSubcribe"><?echo($aVideosEncontrados['nombre']) . ' ' . $aVideosEncontrados['apellido_p'] . ' ' . $aVideosEncontrados['apellido_m']?></span><br>
                                Área del conocimiento: <span class="txtSubcribe"</span><?echo ($aVideosEncontrados['areaConocimiento'])?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
