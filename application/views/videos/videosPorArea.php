

<div class="nine wide column center buscadorVideos">
    <div class="ui fluid icon input buscadorSegundo">
        <input id="buscadorDown" placeholder="Busca por tema, investigador o disciplina" type="text">
        <i id="btnBuscadorDown" class="search link icon"></i>
    </div>
</div>

<div id="contenido" class="ui container">
    <div id="mensajeBusqueda" class="ui message large">
        <p>Hay <strong><?echo( $aVideosAreas != false ? count($aVideosAreas) : 0)?></strong> video(s) en el area del conocimiento '<?echo($sAreaConocimiento)?>' <strong><?echo($busqueda['palabra'])?></strong></p>
    </div>
    <div class=" txtContPHD">
        <div>
            <p><? echo ($aVideosAreas[0]['areaConocimiento'])?></p>
        </div>
    </div>

    <div class="ui stackable three column grid container left aligned">
        <?foreach($aVideosAreas as $aVideo){?>
            <?$aIdVideo = explode("=", $aVideo['liga_youtube'])?>
            <div class="column">
                <div id="cardImg" class="ui card aligned center">
                    <a class="image" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideo['id'])?>">
                        <img class="imagPhd" src="https://img.youtube.com/vi/<?echo($aIdVideo[1])?>/mqdefault.jpg">
                    </a>
                    <div id="datosVideos" class="content left ">
                        <a class="header" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideo['id'])?>">“<?echo($aVideo['titulo'])?>”</a>
                        <div class="description">
                            <p>Investigador: <span class="txtSubcribe"><?echo($aVideo['nombre']) . ' ' . $aVideo['apellido_p'] . ' ' . $aVideo['apellido_m']?></span><br>
                                Área del conocimiento: <span class="txtSubcribe"</span><?echo ($aVideo['areaConocimiento'])?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
    
</div>

