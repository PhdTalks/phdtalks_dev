<?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
    <p class="ui tituloBarra azulMarino"> Agregar video para investigador</p>
<?else:?>
    <p class="ui tituloBarra azulMarino"><?echo ($bActualizar ? 'Actualizar' : 'Agregar') ?> video</p>
<?endif;?>
<div class="fondo gris">

    <div class="ui container">
        <?if($oErrors):?>
            <div class="ui error message">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oErrors->mensajes as $sError):?>
                        <li><? echo($sError)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>

        <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
            <div class="ui success message">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oNotices as $sNotice):?>
                        <li><? echo($sNotice)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>
    </div>

    <form id="formdatosvideos" class="ui container form" method="POST" action="">

        <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
            <div class="field">
                <label>Investigador*</label>
                <select class="ui search dropdown" name="form[idInvestigador]" >
                    <option value="<? echo $aVideo['idInvestigador'];?>">Investigador</option>
                    <?foreach($aInvestigadores as $aInvestigador){?>
                        <option value="<? echo $aInvestigador['id'];?>"><?echo $aInvestigador['nombre']?></option>
                    <?}?>
                </select>
            </div>
        <?endif;?>

        <div class="field">
            <label>Título*</label>
            <input id="inputTitulo" type="text" name="form[titulo]" placeholder="Titulo" value="<?echo($aVideo['titulo'])?>">
        </div>

        <div class="field">
            <label>Área del conocimiento*</label>
            <select class="ui search dropdown" id="areasConocimiento" name="form[subareaConocimiento]" >
                <option value="<? echo $aVideo['id_subarea_conocimiento'];?>">Área del conocimiento</option>
                <?foreach($aSubAreasConocimiento as $subArea){?>
                    <option value="<? echo $subArea['id'];?>"><?echo $subArea['area_nombre']?> / <? echo $subArea['subarea_nombre'];?></option>
                <?}?>
            </select>
        </div>

        <div class="field">
            <label>Journal</label>
            <input id="inputJournal" type="text" name="form[journal]" placeholder="Journal" value="<?echo($aVideo['journal'])?>">
        </div>

        <div class="field">
            <label>Descripción de tu artículo a manera de resumen, en español y en un lenguaje no académico*</label>
            <textarea id="inputDescripcion" type="text" name="form[descripcion]" placeholder="Descripción" rows="4" ><?echo($aVideo['descripcion'])?></textarea>
        </div>
        <div class="field">
            <label>Liga Youtube*</label>
            <input id="inputLigaYoutube" type="text" name="form[liga_youtube]" placeholder="Liga de Youtube" value="<?echo($aVideo['liga_youtube'])?>">
        </div>

        <div class="field">
            <label>Liga al Articulo*</label>
            <input id="inputLigaArticulo" type="text" name="form[liga_articulo]" placeholder="Liga Articulo" value="<?echo($aVideo['liga_articulo'])?>">
        </div>

        <div class="field">
            <label>Coautores</label>
            <input id="inputLigaArticulo" type="text" name="form[coautores]" placeholder="Coautores" value="<?echo($aVideo['coautores'])?>">
        </div>

        <div class="field">
            <label>Palabras Clave:</label>
            <input id="inputPalabraClave" type="text" name="form[palabras_clave]" placeholder="Palabra Clave" value="<?echo($aVideo['palabras_clave'])?>">
            <p>Separe cada Palabra Clave con una coma, por ejemplo: gestión, base tecnológica, humanismo</p>
        </div>

        <?if(!$bActualizar):?>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="checkbox">
                    <label>Estoy de acuerdo con el <a href="<?echo($config->get('baseUrl'))?>avisoLegal" target="_blank">Aviso Legal</a> y el <a href="<?echo($config->get('baseUrl'))?>avisoPrivacidad" target="_blank">Aviso de Privacidad.</a></label>
                </div>
            </div>
        <?endif;?>

        <?if(Session::get('idPerfil') == 1 && $bActualizar):?>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" value="1" name="form[visible]" <?echo($aVideo['visible'] == '0' ? 'checked' : '')?>>
                    <label>Ocultar este video</label>
                </div>
            </div>
        <?endif;?>

        <div class="center">
            <p>Los campos marcados con * son obligatorios.</p>
            <button class="ui button agregarVideo btnAzul" type="submit">Guardar</button>
        </div>

        <div class="ui error message"></div>

    </form>
</div>


<script>
    $(document).ready(function() {
        $(".dropdown").dropdown();

        $('#formdatosvideos').form({
            on: 'blur',
            fields:{
                titulo:{
                    identifier: 'form[titulo]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'Debe especificar el título'
                        }
                    ]
                },
                subareaConocimiento:{
                    identifier: 'form[subareaConocimiento]',
                    rules:[
                        {
                            type:'empty',
                            prompt : 'Debe especificar la área del conocimiento'
                        }
                    ]
                },
                descripcion:{
                    identifier: 'form[descripcion]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La descripción del video no es válida'
                        }
                    ]
                },
                ligaYoutube:{
                    identifier: 'form[liga_youtube]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La liga de Youtube no es válida'
                        }
                    ]
                },
                ligaArticulo:{
                    identifier: 'form[liga_articulo]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La liga del artículo no es válida'
                        }
                    ]
                },
                checkbox:{
                    identifier: 'checkbox',
                    rules:[
                        {
                            type   : 'checked',
                            prompt : 'Debe aceptar el aviso de privacidad y aviso legal'
                        }
                    ]
                }
                <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
                ,
                investigador:{
                    identifier: 'form[investigador]',
                    rules:[
                        {
                            type:'empty',
                            prompt : 'Debe especificar un investigador'
                        }
                    ]
                },
                <?endif;?>
            }
        });
    });
</script>