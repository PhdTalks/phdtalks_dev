<div id="adminPhd">
    <h1 class="center aligned azulMarino">Bienvenid@ <?echo(Session::get('nombre'))?></h1>

        <div id="menuGeneralAdmin" class="ui container">
            <div  class="ui grid stackable centered">
                <div  class="five wide column contenedorBtn">
                    <div class=" blueBackground">
                        <div class="header txtHeader">Investigadores</div>
                    </div>
                    <div class="ui items">
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/investigadores/registrar"><i class="icon add"></i>Registrar nuevo investigador</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/investigadores/listado"><i class="icon list"></i>Administrar Investigadores</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="five wide column contenedorBtn">
                    <div class="ui items">
                        <div class="content blueBackground">
                            <div class="header txtHeader">Administradores</div>
                        </div>
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/usuarios/registrar"><i class="icon add"></i>Registrar nuevo Administrador</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/usuarios/listado"><i class="icon list"></i>Administrar usuarios Administradores</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="five wide column contenedorBtn">
                    <div class="ui items">
                        <div class="content blueBackground">
                            <div class="header txtHeader">Videos</div>
                        </div>
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content ">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/videos/listado"><i class="icon list"></i>Administrar Videos </a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/video/agregar"><i class="icon add"></i>Agregar video para investigador </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

</div>