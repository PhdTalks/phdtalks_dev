<p class="ui tituloBarra azulMarino">Acerca de nosotros</p>


<div id="acercaDe">

    <div id="contenedorAcercaDe" class="ui container">

        <div class="row">
            <div id="videoAcercaDe" class="center aligned">
                <div class="video-responsive">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/D4MSfqj5sVw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>



        <div id="txtAcercaDe">
            <div id="textoJustificado">
                <h4>¿Qué es Phd Talks?</h4>
                <p>Phd Talks es una iniciativa que busca poner la ciencia al alcance de todos, para lo cual utiliza una plataforma web en la que investigadores en distintas áreas de conocimiento pueden difundir sus artículos de investigación publicados en revistas científicas de calidad utilizando un formato de video que es amigable, ágil y en idioma español.</p>

                <h4>¿Hacia dónde vamos?</h4>
                <p>PhD Talks será el observatorio de artículos científicos en formato de video-resumen más completo, preferido y utilizado por nuestros usuarios de habla hispana.</p>
            </div>
        </div>

    </div>

</div>
