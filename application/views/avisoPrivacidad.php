<p class="ui tituloBarra azulMarino">Aviso de privacidad</p>

<div class="ui container">

    <div class="textoJustificado">

        <p>

            <br>
            R.F.C. XAXX010101000 <br>
            <br>

            De conformidad con lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y en su Reglamento, hace de su conocimiento que es responsable de recabar sus datos personales, del uso que se les dé a los mismos y de su protección. Su información personal será para uso exclusivo de la “empresa”, de misma manera para informarle sobre condiciones y cambios en los mismos, así como para evaluar la calidad del servicio que le ofrecemos.

            Para las finalidades antes mencionadas requerimos obtener sus siguientes datos personales: <br>

            <br>
            •	Nombre <br>
            •	Apellido Paterno <br>
            •	Apellido Materno <br>
            •	Fecha de nacimiento <br>
            •	Nacionalidad <br>
            •	Sexo <br>
            •	Correo electrónico Institucional <br>
            •	Correo electrónico Personal <br>
            •	Número de Registro CVU <br>
            •	Institución Académica o de Investigación <br>
            •	País de la Institución <br>
            •	Nombramiento <br>
            •	Contraseña <br>
            <br>



            Tratamiento de sus Datos Personales: <br>
            <br>

            Recabamos sus datos personales para los efectos mencionados en presente Aviso de Privacidad.

            En este sentido, hacemos de su conocimiento que sus datos personales serán tratados y resguardados con base en los principios de licitud, calidad, consentimiento, información, finalidad, lealtad, proporcionalidad y responsabilidad, consagrados en la Ley Federal de Protección de Datos Personales en Posesión de Particulares y su Reglamento.

            Finalidad en el tratamiento de sus datos personales:

            Recopilamos sus datos personales, por lo que solicitamos su consentimiento a través del presente Aviso de Privacidad con el objeto de utilizarlos para los siguientes fines:<br>

            <br>
            1. Para generar un perfil de usuario con los datos que nos ha confiado<br>
            2. Identificarlo como cliente y prestarle los servicios que puede brindar “La Empresa”<br>
            3. Para contactarlo y enviarle información relevante respecto de temas de nuestros servicios.<br>
            <br>

            Medios para limitar el uso o divulgación de sus datos personales:<br>
            <br>

            Hacemos de su conocimiento que sus datos personales serán resguardados bajo estrictas medidas de seguridad administrativas, técnicas y físicas las cuales han sido implementadas con el objeto de proteger sus datos personales contra daño, pérdida, alteración, destrucción o el uso, acceso o tratamiento no autorizados.

            Medios para el ejercicio de sus derechos como titular de los datos personales:

            Como titular de los datos personales objeto del presente Aviso de Privacidad usted podrá ejercitar sus derechos de acceso, rectificación, cancelación u oposición (Derechos ARCO), mismos que se consagran en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y en su Reglamento.

            Así mismo podrá revocar el consentimiento otorgado para el uso de sus datos personales.

            En cualquiera de estos supuestos, puede realizar su solicitud mediante el envío de un correo electrónico a la dirección  contacto@phdtalks.com o solicitarlo mediante nuestra página de internet www.phdtalks.com.

            Con la finalidad de poder atender su solicitud, ésta deberá satisfacer todos los requisitos estipulados en la Ley Federal de Protección de Datos Personales en Posesión de los particulares y en su Reglamento.

            Cambios al presente Aviso de Privacidad:

            El presente Aviso de Privacidad podrá ser modificado en el futuro. En todo caso, cualquier modificación al mismo se hará de su conocimiento mediante el envío de un correo electrónico a la cuenta que usted nos proporcionó inicialmente para hacer de su conocimiento el presente Aviso de Privacidad y/o mediante la publicación del mismo en la siguiente página web: www.phdtalks.com.

            Si usted no manifiesta su oposición para que sus datos personales sean transferidos, se entenderá que ha otorgado su consentimiento para ello.<br>

            <br>
            • Si consiento que mis datos personales sean transferidos en los términos que señala el presente aviso de privacidad.<br>
            • No consiento que mis datos personales sean transferidos en los términos que señala el presente aviso de privacidad.<br>
            <br>

            Si usted desea dejar de recibir mensajes promocionales de nuestra parte puede solicitarlo vía correo electrónico  contacto@phdtalks.com o visitar nuestra página de internet www.phdtalks.com<br>

            <br>
            Fecha de última actualización 24 de abril de 2017.<br>
            <br>

            No seremos responsables en caso de que usted no reciba la referida notificación de cambio en el Aviso de Privacidad por causa de algún problema con su cuenta de correo electrónico o de transmisión de datos por internet.

            Sin embargo, por su seguridad, el Aviso de Privacidad vigente estará disponible en todo momento en la página web antes señalada.


        </p>
    </div>

</div>


