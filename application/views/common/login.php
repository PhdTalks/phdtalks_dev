<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>PHD Talks - Portal del artículos científicos</title>

    <!--Archivos relacionados-->
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/admin.css">
    <script src="<?echo($config->get('baseUrl'))?>assets/js/jquery.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/semantic.min.js"></script>
</head>
<body id="loginBody">
<?php $this->show($sSectionFile, get_defined_vars()['vars'])?>
</body>
</html>