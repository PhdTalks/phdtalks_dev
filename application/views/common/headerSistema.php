<div id="fixedmenu" class="ui top fixed">
    <div id="barraMenuAdmin" class="ui borderless main menu">
        <div class="logoPHD">
            <a href="<?echo($config->get('baseUrl'))?>"><img src="/assets/img/logo.png"></a>
        </div>

        <div id="dropdownMenu" class="right menu">
            <div class="ui simple dropdown item dropSimpleMenu">
                <i class="ui icon large user"></i>
                <p class="nomUsuarioPHD"><?echo(Session::get('nombre')); echo(Session::get('idPerfil') == '1' ? ' (Administrador)' : ' (Investigador)')?></p>
                <i id="flechaDropdown" class="dropdown icon"></i> <!--nuevo-->
                <div class="ui secondary vertical pointing menu iteMenuAdmin">
                    <?if(Session::get('idPerfil') == '1'): ?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>admin/misdatos">Actualizar mi Perfil</a>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>admin/index">Panel Administrativo</a>
                    <?else:?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/misdatos">Actualizar mi Perfil</a>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/index">Administrar mis videos</a>
                    <?endif;?>
                    <a class="item" href="<?echo($config->get('baseUrl'))?>logout">Salir</a>
                </div>
            </div>
        </div>
    </div>

    <div id="menuP">
        <div class="ui breadcrumb">
            <?foreach($aNavegacion as $sUrl => $sTitulo){?>
                <?if($sUrl){?>
                    <?if((Session::get('idPerfil')) == '1'):?>
                        <a class="section" href="<?echo ($config->get('baseUrl'))?>admin/index" class="section"><?echo $sTitulo?></a>
                    <?else:?>
                        <a class="section" href="<?echo ($config->get('baseUrl'))?>investigador/index" class="section"><?echo $sTitulo?></a>
                    <?endif;?>
                    <a class="right angle icon divider"> > </a>
                <?}else{?>
                    <div class="active section"><?echo($sTitulo)?></div>
                <?}?>
            <?}?>
        </div>
    </div>
</div>

