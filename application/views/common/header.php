<div id="fixedmenu" class="ui top fixed">
    <div id="barraMenu" class="ui fluid three menu">
        <div class="logoPHD">
            <a href="<?echo($config->get('baseUrl'))?>"><img src="/assets/img/logo.png"></a>
        </div>

        <div class="inputBuscar">
            <div id="buscadorMenu" class="ui fluid icon input buscadorSegundo">
                <input id="buscadorUp" class="inputBuscador" placeholder="Busca por tema, investigador o disciplina" type="text">
                <i id="btnBuscadorUp" class="btnBuscador search link icon"></i>
            </div>
        </div>

        <?if(Session::get('idUsuario')){?>
            <div id="dropdownMenu" class="right menu">
                <div class="ui simple dropdown item dropSimpleMenu">
                    <i class="ui icon large user"></i>
                    <p class="nomUsuarioPHD"><?echo(Session::get('nombre')); echo(Session::get('idPerfil') == '1' ? ' (Administrador)' : ' (Investigador)')?></p>
                    <i class="dropdown icon"></i>
                    <div class="ui secondary vertical pointing menu iteMenuAdmin">
                        <?if(Session::get('idPerfil') == '1'): ?>
                            <a class="item" href="<?echo($config->get('baseUrl'))?>admin/misdatos">Actualizar mi Perfil</a>
                            <a class="item" href="<?echo($config->get('baseUrl'))?>admin/index">Panel Administrativo</a>
                        <?else:?>
                            <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/misdatos">Actualizar mi Perfil</a>
                            <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/index">Administrar mis videos</a>
                        <?endif;?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>logout">Salir</a>
                    </div>
                </div>
            </div>
        <?}else{?>
        <div class="right menu">
            <a href="<?echo($config->get('baseUrl'))?>login">
                <div class="nomUsuarioPHD alignedRight">
                    <p>Iniciar Sesión</p>
                </div>
            </a>
        </div>
        <?}?>
    </div>

    <div class="ui borderless menu" id="menuP">
        <div class="header item txtMenu txtFiltrar">
            Filtrar videos por:
        </div>

        <a href="<?echo($config->get('baseUrl'))?>" class="item txtMenu txtDatosMenu">
            <i><img class="clock" src="/assets/img/clock.png"></i>
            <div class="lineaOption">
                Videos más recientes
            </div>
        </a>

        <div id="menuAreas" class="ui floating dropdown link item txtDatosMenu">
            <span>
                <img src="/assets/img/areas.png">
                <div class="lineaOption2"> Áreas del Conocimiento</div>
            </span>
            <i class="optMenuFecha">
                <img src="/assets/img/flechaOpcionMenu.png">
            </i>
            <div class="menu">
                <? foreach($areasConocimiento as $area) {?>
                    <a href="<?echo($config->get('baseUrl'))?>videos/area?id=<?echo($area['id'])?>" class="item optMenu2"><? echo $area['nombre'];?></a>
                <?}?>
            </div>
        </div>

        <div id="itemTutoriales" class="ui right menu">
            <div class="item">
                <a href="<?echo($config->get('baseUrl'))?>tutoriales">Tutoriales</a>
            </div>
        </div>


        <div id="click" class="ui floating cel">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/Cel.png">
        </div>
        <div class="ui left demo sidebar vertical inverted menu" id="sidebarMenu2">
            <a class="item" href="">Videos más recientes</a>
            <div class="item">
                <b>Áreas del Conocimiento</b>
                <div class="menu">
                    <? foreach($areasConocimiento as $area) {?>
                        <a href="<?echo($config->get('baseUrl'))?>videos/area?id=<?echo($area['id'])?>" class="item optMenu2"><? echo $area['nombre'];?></a>
                    <?}?>
                </div>
            </div>
        </div>
    </div>


    <?if(!Session::get('idUsuario') and $bFlag!=1){?>
        <!--Si no esta en el usuario-->
        <div id="suscribete" class="column center aligned">
            <p class="txtSubcribe">¿Eres Investigador y deseas publicar tus videos en PHDTalks?
                <a href="<?echo($config->get('baseUrl'))?>investigador/registrar" id="colorRegistro">Regístrate aquí</a>
            </p>
        </div>
    <?}?>
</div>


<script type="application/javascript">
    $(document).ready(function() {

        semantic.menu.ready();

        $('#click').click(function()
            {
                $('.left.demo.sidebar')
                    .sidebar('toggle')
            }
        );
    });
</script>