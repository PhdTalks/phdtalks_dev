<p class="ui tituloBarra azulMarino">Aviso Legal</p>

<div class="ui container">

    <div class="textoJustificado">
        <p>
            <br>
            CONTRATO DE PRESTACIÓN DE SERVICIOS QUE CELEBRAN POR UNA PARTE “USTED” QUE EN LO SUCESIVO SE LE DENOMINARÁ “EL USUARIO” Y POR LA OTRA PARTE “PHD TALKS” QUE EN LOS SUCESIVO SE LE DENOMINARÁ “LA EMPRESA” LAS CUALES SE SUJETAN AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CLAUSULAS. <br>
            <br>
            Declara “LA EMPRESA” <br>
            <br>
            Que es una persona moral debidamente constituida conforme a la legislación mexicana. Que tiene la capacidad legal, tecnológica y humana para celebrar el presente contrato, facultades que no han sido restringidas o modificadas en forma alguna. Que la clave de su Registro Federal de Contribuyentes ante la Secretaría de Hacienda y Crédito Público es SAIA720717253 conforme a su cédula de identificación fiscal.<br>

            <br>
            Declara “EL USUARIO” <br>
            <br>
            Que los datos proporcionados en el formulario de inscripción son fidedignos, por lo que manifiesta que la información que proporciona es real y comprobable, relevando a “LA EMPRESA” de cualquier responsabilidad por la falsedad con la que se conduzca, por la falsificación de documentos o suplantación de personas, nombres, direcciones o datos de identificación que utilice. Que se encuentra en pleno uso de sus facultades físicas, mentales y legales para poder celebrar este contrato. “EL USUARIO” garantiza que tiene más de18 años de edad y que los documentos que publique en la página web son de su autoría. Que es el único y exclusivo responsable por los datos, marcas, logos, patentes o símbolos y colores de identificación que proporciona.

            Condiciones de uso
            Al momento en el que el “usuario” continúa navegando y haciendo uso de esta plataforma, acepta cumplir y estar obligado por los siguientes Términos y Condiciones de uso, que junto con la Política de Privacidad gobiernan la relación del “usuario” y la “empresa”.

            En caso de no estar de acuerdo con alguna parte de los términos y condiciones, favor de no utilizar el sitio web. Si decide continuar navegando y utilizando este sitio, usted está aceptando con ello este acuerdo. <br>

            <br>
            Limitaciones de Uso <br>
            <br>
            El “usuario” no puede copiar, derivar, editar, traducir, descompilar, realizar ingeniería inversa, modificar, usar, o reproducir cualquier código o fuente de código relativo al sitio Web.
            El acceso y servicios a través del Sitio está sujeta a cambios y sin compromiso. Nos reservamos el derecho de limitar el acceso a este Sitio , ya sea temporal o permanente , o cambios en cualquier momento, con o sin previo aviso.

            La “empresa” no ofrece ninguna garantía en cuanto a la exactitud, puntualidad, rendimiento, integridad o adecuación de la información o materiales encontrados u ofrecidos en este sitio para cualquier propósito particular. El “usuario” reconoce que dicha información y materiales pueden contener inexactitudes o errores. Por tal motivo, la “empresa” excluye de la responsabilidad por cualquier inexactitud o errores.

            El uso de cualquier información o material contenido en este sitio web es bajo riesgo del “usuario”, de manera tal que la “empresa” no se hace responsable de la misma.
            Será responsabilidad del “colaborador” el presentar información o investigación verídica y de su autoría.
            La “empresa” no somete a juicio la información presentada por los “colaboradores”, por tal motivo, la “empresa” se reserva el derecho, pero no la responsabilidad, de editar o eliminar cualquier elemento subido a la plataforma por los “colaboradores”, incluyendo los considerados por nosotros para infringir estos términos y condiciones. En caso de denuncia por derechos de autor, la “empresa” hará todo los esfuerzos para investigar los hechos pero no ofrece garantía alguna.

            De misma manera la empresa no tiene ningún tipo de responsabilidad para editar, eliminar o seguir permitiendo la visualización de ninguna de las Entregas que sea.

            Este sitio web contiene material que es propiedad de o licenciadas a terceros. Este material incluye, pero no está limitado a, el diseño, presentación, apariencia y gráficos. La reproducción está prohibida salvo de conformidad con la nota de copyright, que forma parte de estos términos y condiciones.

            Todas las marcas reproducidas en esta página web, que no son propiedad de, o con licencia de terceros, son reconocidas por el sitio web.

            En algunos casos, la plataforma puede incluir enlaces a otros sitios web. Estos vínculos se proporcionan para su conveniencia para proporcionar más información. Esto no significa que la “empresa” se encuentre de acuerdo con la información del tercero. Por tanto, no tenemos ninguna responsabilidad por el contenido del sitio web vinculado.

            En los foros se busca una comunicación abierta y respetuosa de tal manera que no la “empresa” no tolerará ofensas, insultos, faltas de respeto, discriminación u otras declaraciones inapropiadas, así como material o enlaces.

            Este tipo de ofensas incluyen, más no se limitan a aquellos actos que sean calumniosos, difamatorios, indecentes, dañinos, acosadores, intimidantes, amenazantes, odiosos, ofensivos, discriminatorios, abusivos, vulgares, obscenos, pornográficos, sexualmente explícitos u ofensivo en cualquier sentido sexual, racial, cultural o étnico.

            Ningún “colaborador” puede presentar material que no sea de su autoría. De igual manera no se debe publicar material ilegal, inapropiado difamatorio u otro. El material presentado debe ser propio del “colaborador” o un trabajo al que tenga una licencia de uso. Al momento de subir material a la plataforma el “colaborador” manifiesta y garantiza que posee o controla todos los derechos sobre la información contenida.<br>
            <br>

            Información Personal <br>

            <br>

            La información personal sobre los visitantes de nuestro sitio sólo se recoge cuando a sabiendas y voluntariamente es presentada.

            De conformidad con lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y en su Reglamento, hace de su cocimiento que cuando la información es solicitada, la “empresa” es responsable de recabar sus datos personales, del uso que se les dé a los mismos y de su protección.<br>
            <br>
            Uso de la Información <br>
            <br>
            La información personal que presenten los visitantes a nuestro sitio se utiliza únicamente para la finalidad para la que se presenta o para los demás efectos secundarios que están relacionados con el objetivo principal, a menos que revelemos otros usos en el Aviso de Privacidad o al momento a en situaciones especiales cuando tenemos razones para creer que ello es necesario para identificar, contactar o tomar acción legal en contra de cualquier persona que dañe, perjudique, o interfiera (intencionalmente o no) con nuestros derechos o propiedad, los usuarios, o cualquier otra persona que podría ser perjudicada por dichas actividades.

            Además, podemos divulgar información personal cuando la ley bajo mandato escrito exija dicha divulgación.<br>
            <br>
            Seguridad<br>
            <br>
            “La empresa” se esfuerza para garantizar la seguridad, integridad y privacidad de la información personal enviada a nuestros sitios, de manera que se revisa y actualizan las medidas de seguridad a la luz de las tecnologías actuales. Desafortunadamente, ninguna transmisión de datos a través de Internet puede ser garantizada  como totalmente segura. Sin embargo, nos esforzaremos por adoptar todas las medidas razonables para proteger la información personal que usted pueda transmitirnos o de nuestros productos y servicios en línea. Una vez que recibimos su transmisión, también vamos a hacer nuestros mejores esfuerzos para garantizar su seguridad en nuestros sistemas. Además, nuestros empleados y los contratistas que proporcionan servicios relacionados con nuestros sistemas de información están obligados a respetar la confidencialidad de cualquier información personal en poder nuestro. Sin embargo, no nos haremos responsable por los acontecimientos derivados del acceso no autorizado a su información personal.

            Propiedad intelectual, marca registrada y derechos de autor

            “EL USUARIO” declara que los contenidos que coloque en el espacio contratado en el servidor de “LA EMPRESA”, sean estos nombres propios,
            marcas de productos y/o servicios, imágenes de cualquier naturaleza, citas, referencias, archivos con sonidos o videos u otros materiales, son de su propiedad o cuentan con la autorización correspondiente de sus legítimos propietarios. En todo caso,
            “EL USUARIO” será el único responsable por cualquier transgresión a lo declarado en la presente cláusula, quedando “LA EMPRESA” libre de cualquier responsabilidad ya que sólo se limita a publicar los contenidos que “EL USUARIO” sube a la página web. “EL USUARIO” se compromete a observar y respetar lo dispuesto por los artículos 2, 9, 10, 10 BIS, 11, 12, 15, 22, 27, 28, 31, 62, 87, 88, 89, 98 BIS y 100 de la Ley de la Propiedad Industrial Vigente en México, y sus correlativos en las disposiciones legales en los países extranjeros. “EL USUARIO” no podrá hacer uso,
            ni de manera parcial o total de las patentes, registros de modelos de utilidad, diseños industriales, marcas, y avisos comerciales de “LA EMPRESA”. “EL USUARIO” no podrá hacer uso de las denominaciones, signos, símbolos, siglas o emblemas de “LA EMPRESA” como si fueran propias. “EL USUARIO” no podrá realizar actos que atenten contra la propiedad industrial o que constituyan competencia desleal relacionada con la misma. “EL USUARIO” no podrá hacer uso de marcas de “LA EMPRESA” quien tiene el derecho de uso exclusivo, entendiéndose por marca a todo signo visible que distinga los productos o servicios de “LA EMPRESA”. “EL USUARIO” es el único y exclusivo responsable por la veracidad y legalidad de los datos que proporciona a “LA EMPRESA”, incluyendo en esto su marca, patente o registro que lo identifique, liberando a “LA EMPRESA” de cualquier responsabilidad civil, penal o administrativa por el uso o dominio de marcas o patentes en grado de confusión con cualquier otra registrada en México o en el extranjero. Así mismo “EL USUARIO“ manifiesta quedar enterado y acepta responsabilidad por violaciones de hecho o de derecho que establezcan
            los organismos nacionales e internacionales en tratándose de propiedad intelectual, obligándose a respetar las determinaciones y resoluciones que de ellas emanen en lo relativo a la propiedad de un dominio, obligándose de igual forma a respetar los procedimientos y resoluciones que establezca “LA EMPRESA” en lo referente a los procedimientos de resolución de disputas. “EL USUARIO” es el único y exclusivo responsables de las sanciones o infracciones que impongan las autoridades en materia de propiedad industrial y autoridades civiles y/o penales. En caso de conflicto por marcas, patentes, registros de modelos de utilidad, diseños industriales, marcas, y avisos comerciales, “LA EMPRESA” se reserva el derecho de suspender temporalmente el servicio para efectuar la aclaración correspondiente con “EL USUARIO”, sin ninguna responsabilidad para “LA EMPRESA” por dicha suspensión temporal.<br>

            <br>
            Acceso a la Información <br>
            <br>
            La “empresa” se esforzará por adoptar todas las medidas razonables para mantener segura la información que tenemos acerca de usted, y para mantener esta información exacta y actualizada. Si, en cualquier momento, usted descubre que la información que poseemos sobre usted es incorrecta, puede ponerse en contacto con nosotros para corregir dicha información.  Además, nuestros empleados y los contratistas que proporcionan servicios relacionados con nuestros sistemas de información están obligados a respetar la confidencialidad de cualquier información personal en nuestro poder.

            La solicitud de corrección parcial o total de datos personales, sólo podrá ser formulada por el interesado titular de los mismos o su representante legal debidamente acreditado.<br>
            <br>
            Enlaces a otros sitios<br>
            <br>
            En algunos casos la “empresa” o el “colaborador” ofrece enlaces a sitios Web fuera de la plataforma. Estos sitios enlazados no están bajo nuestro control, y no podemos aceptar responsabilidad por la conducta de otras empresas. Antes de revelar su información personal en cualquier otro sitio Web, le sugerimos examinar los términos y condiciones de uso de dicho sitio Web, así como su aviso de privacidad.<br>

            <br>
            Diverso <br>
            <br>
            Los presentes Términos y Condiciones, junto con el propietario de estos sitios Web, constituyen el acuerdo completo entre las partes en relación con el uso de este sitio. Si alguna disposición de estos Términos y Condiciones es considerada nula, ilegal o inaplicable por cualquier razón, dicha disposición será separada del resto de los términos y condiciones y no afectará la validez y aplicabilidad de ninguna de las disposiciones restantes.

            En cualquier momento podremos modificar, añadir o eliminar cualquier parte de este acuerdo o cualquier parte de los servicios y funciones previstas en el Sitio. En caso de ser así, se actualizará y publicará el acuerdo en la plataforma. El propietario de estos sitios Web puede revisar estos Términos y Condiciones en cualquier momento actualizando esta publicación y dichos cambios serán efectivos inmediatamente a menos que se indique lo contrario.

            Si cualquier cambio futuro es inaceptable para el “usuario”, el “usuario” debe suspender el uso del Sitio. Su uso continuado del Sitio indica e implica la aceptación de este acuerdo y de las modificaciones del mismo. Si no está de acuerdo con alguna parte de estos términos y condiciones, por favor no haga uso de la plataforma.

            Ley
            En caso de disputa entre ”El USUARIO” y ”LA EMPRESA”, las partes están conscientes y de acuerdo en someterse a los Tribunales y las leyes sustantivas y adjetivas del Estado de Jalisco, México, para dirimir sus diferencias. Por lo que renuncian a cualquier fuero y legislación que les pudiera ser aplicable por razón de su domicilio presente o futuro o bien por cualquier otra causa. Las partes asumen competencia en los términos de lo previsto por el Título Tercero, Capítulo Primero del Código de Procedimientos Civiles para el Estado de Jalisco. Los títulos o definiciones que se incluyen en este acuerdo son para conveniencia solamente y no afectarán la construcción o la interpretación de este acuerdo. “EL USUARIO” al momento de registrarse como usuario, está de acuerdo a que estos actos se consideren como su consentimiento expreso, aceptando y conociendo los significados y alcances de los términos y condiciones aquí plasmados. En caso de incurrir en abusos y/o actividades ilegales que requieran procedimientos legales, estos deberán ser realizados en primera instancia, sometiendo expresamente jurisdicción a los tribunales y autoridades competentes en el Estado de Jalisco, México. “EL USUARIO” será el único responsable de las actividades que desarrolle al amparo de nuestros servicios o productos, relevando a “LA EMPRESA” de cualquier responsabilidad por ello. En caso de que “LA EMPRESA” se vea involucrada en procedimientos judiciales o administrativos por las conductas o actividades que despliegue “EL USUARIO”, este último se hará responsable de las costas y gastos que ello origine.
        </p>

    </div>

</div>
