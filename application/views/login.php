<div id="adminLogin" class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui image medium">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/admin/loginLogo.png">
        </h2>
        <form id="formLogin" class="ui large inverted form" method="POST" action="">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="mail outline icon"></i>
                        <input id="inputEmail" type="text" name="form[email]" placeholder="Correo electrónico personal">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="form[password]" placeholder="Contraseña">
                    </div>
                </div>
                <div class="ui large yellow submit button">
                    <i class="sign in icon"></i> Ingresar a PHD Talks
                </div>

            <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
                <div id="mensajeExitoLogin" class="ui success message messagePerfil" style="display:  <?php echo($oNotices ? 'block' : '')?>">
                    <i class="close icon"></i>
                    <ul class="list">
                        <?foreach($oNotices as $sNotice):?>
                            <li><? echo($sNotice)?></li>
                        <? endforeach;?>
                    </ul>
                </div>
            <? endif;?>

            <div class="ui error message" style="display: <?php echo($oErrors->mensajes ? 'block' : '')?>">
                <?php if($oErrors->mensajes){?>
                    <ul class="list">
                        <?foreach($oErrors->mensajes as $sMensaje){?>
                            <li><?echo($sMensaje)?></li>
                        <?}?>
                    </ul>
                <?}?>
            </div>


        </form>
        <div id="creaCuenta" class="ui segment inverted large">
            Si eres investigador y aún no tienes tu cuenta con nosotros: &nbsp; <a href="<?echo($config->get('baseUrl'))?>investigador/registrar" class="ui button yellow large">Crea tu cuenta</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.ui.form').form({
            fields: {
                email: {
                    identifier  : 'form[email]',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Especifique el correo electrónico'
                        }
                    ]
                },
                password: {
                    identifier  : 'form[password]',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Especifique la contraseña'
                        }
                    ]
                }
            }
        });

        $('#inputUsuario').focus();
    });
</script>