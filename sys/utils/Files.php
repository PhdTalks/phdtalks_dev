<?php
/**
 * Clases usauda para el manejo de los arcivos
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package utils\Files
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */

require_once $config->get('modelsFolder').'sistema/SysTiposArchivos.php';
require_once $config->get('modelsFolder').'organizador/OrgArchivos.php';

class Files {
    
    /**
     * Objeto Config
     * 
     * @var Config 
     */
    public $_config;
    
    /**
     * @var
     */
    private $_oTipoArchivo = null;
    
    /**
     * @var
     */
    private $_oArchivo = null;

    /**
     * Contructor de la clase
     */
    public function __construct()
    {
        $this->_config = Config::getInstance();
    }
    
    /**
     * Carga un nuevo registro en la carpeta correspondiente al tipo
     * con el nombre configurado en la tabla de sys_tiposArchivos y
     * agrega un registro en org_archivos
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @param array $aFile
     * @param bool $bSobreEscribir
     * @return bool
     */
    protected function subirArchivo($sTipo, $nIdEntidad, $aFile, $bSobreEscribir = false)
    {
        $this->validarArchivo($sTipo, $aFile);
        
        $sFolderCont= $this->getFolderContent($nIdEntidad);
        
        $sPathEntidad = $this->getFolderArchivo($sTipo, $nIdEntidad, $sFolderCont);
        $sNombreArchivo = $this->getNombreArchivo($sTipo, $aFile['name']);
        
        if(!is_dir($sPathEntidad)) {
            
            $this->createFolder($sPathEntidad);
        }
        
        $sPathArchivo = $sPathEntidad . $sNombreArchivo;
        
        //Se valida si ya existe un archivo con el mismo nombre
        if(file_exists($sPathArchivo)) {
            
            if($bSobreEscribir === false) {
            
                $sMsj = "El archivo ya existe.";

                throw new Excepcion('LoadFile', $sMsj, '');
            }
            
            $bResult = $this->eliminarArchivo($sTipo, $nIdEntidad);
            
            /*if(!$bResult) {
                $sMsj = "Error al sobre escribir el archivo.";

                throw new Excepcion('LoadFile', $sMsj, '');
            }*/
        }
        
        if($bResult = $this->subir($aFile['tmp_name'], $sPathArchivo)) {
            
            $bResult = $this->guardarRegistroArchivo($sTipo, $nIdEntidad, $sPathArchivo, $aFile['size']);
        }
        
        return $bResult;
    }
    
    /**
     * Realiza la descarga de un archivo registrado ya regresa la acción
     * completa para la descarga incluyendo las cabeceras correspondientes
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @return boolean
     */
    protected function descargarArchivo($sTipo, $nIdEntidad)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        $oArchivo = $this->getArchivo($oTipoArchivo->ID, $nIdEntidad);
        
        if($oArchivo == false) {
            return false;
        }
        
        if( !file_exists($oArchivo->ruta) ) {
            return false;
        }
        
        $sNombreArchivo = $oTipoArchivo->nombre.'.'.$oArchivo->ext;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $sNombreArchivo);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $oArchivo->size);

        ob_clean();
        flush();
        readfile($oArchivo->ruta);
        exit();
    }
    
    /**
     * Elimina un archivo existente del servidor y su registro en
     * org_archivos
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @param array $aFile
     * @param bool $bSobreEscribir
     * @return bool
     */
    protected function eliminarArchivo($sTipo, $nIdEntidad)
    {
        $sPathArchivo = $this->getPathArchivoExistente($sTipo, $nIdEntidad);
        
        //Se valida si existe el archivo
        /*if(!file_exists($sPathArchivo)) {
            return false;
        }*/
        
        if($bResult = $this->eliminar($sPathArchivo)) {
            
            $bResult = $this->eliminarRegistroArchivo($sTipo, $nIdEntidad);
        }
        
        return $bResult;
    }
    
    /**
     * Valida que el archivo cumpla con los requerimientos
     * 
     * @param string $sTipo
     * @param array $aFile
     * @return bool
     */
    protected function validarArchivo($sTipo, $aFile)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        
        if($oTipoArchivo == false) {
            return false;
        }
        
        $sExt = $this->getExtArchivo($aFile['name']);
        $sTamanoArchivo = $aFile['size'];
        
        $aTypesValidos = explode(',', $oTipoArchivo->ext_validas);
        
        if ( !in_array($sExt, $aTypesValidos) || $sTamanoArchivo > $oTipoArchivo->size_valido ) {
            return false;
        }
        
        return true;
    }


    /**
     * Carga un archivo desde una ubicacion temporal a una definitiva
     * 
     * @param string $sPathTemporal
     * @param string $sPathDefinitivo
     * @return boolean True si el cargado del archivo es exitoso o false en caso contrario
     * @throws Exception
     */
    private function subir($sPathTemporal, $sPathDefinitivo)
    {
        $bResult = false;
        
        if (move_uploaded_file($sPathTemporal, $sPathDefinitivo)){ 

            $bResult = true;
        }
        
        return $bResult;
    }
    
    /**
     * Elimina un archivo en la ubición recibida
     * 
     * @param string $sPathArchivo
     * @return boolean False si falla la eliminación o si el archivo no existe
     * y true si la eliminación es exitosa
     */
    private function eliminar($sPathArchivo)
    {
        $bResult = false;

        if(!file_exists($sPathArchivo)) 
        {   
            //return $bResult;
            return true;
        }
        
        if (unlink($sPathArchivo)) { 

            $bResult = true;
        }
        
        return $bResult;
    }
    
    /**
     * Elimina el registro del archivo
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @return boolean
     */
    private function eliminarRegistroArchivo($sTipo, $nIdEntidad)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        $oArchivo = $this->getArchivo($oTipoArchivo->ID, $nIdEntidad);
        
        
        /*if($oArchivo == false) {
            return false;
        }*/
        
        $bResult = $oArchivo->delete();
        
        return $bResult;
    }

    /**
     * 
     * @param string $sPath
     * @return true
     */
    private function createFolder($sPath)
    {   
        $bResult = mkdir($sPath);
        
        return $bResult;
    }
    
    /**
     * Regresa el número de carpeta contenedora para indice recibido
     * 
     * @param int $nEntidad
     * @return int
     */
    private function getFolderContent ($nEntidad)
    {
        $nFolder = floor($nEntidad / $this->_config->get('maxFilesFolder'));
        
        return $nFolder;
    }
    
    /**
     * Regresa la ruta completa del archivo para guardarse en 
     * el servidor
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @param string $sFolderCont
     * @return string|boolean
     */
    private function getFolderArchivo($sTipo, $nIdEntidad, $sFolderCont)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        
        if($oTipoArchivo == false) {
            return false;
        }
        
        $path1 = $this->_config->get('filesFolder') . $oTipoArchivo->ruta . $sFolderCont;
        if(!file_exists($path1))
        {
            $this->createFolder($path1);
        }
        
        $sPath = $path1 . '/' . $nIdEntidad . '/';
        
        return $sPath;
    }
    
    /**
     * Regresa la ruta completa del archivo para guardarse en 
     * el servidor
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @return string|boolean
     */
    private function getPathArchivoExistente($sTipo, $nIdEntidad)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        $oArchivo = $this->getArchivo($oTipoArchivo->ID, $nIdEntidad);
        
        if($oTipoArchivo == false) {
            return false;
        }
        
        return $oArchivo->ruta;
    }
    
    /**
     * 
     * @param string $sTipo
     * @param string $sName
     * @return string
     */
    private function getNombreArchivo($sTipo, $sName)
    {
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        $sExt = $this->getExtArchivo($sName);
        
        $sNombre = $oTipoArchivo->nombre . '.' . $sExt;
        
        return $sNombre;
    }
    
    /**
     * 
     * @param string $sTipo
     * @return SysTipoArchivos
     */
    private function getTipoArchivo($sTipo)
    {
        if($this->_oTipoArchivo == null || $this->_oTipoArchivo == false || $this->_oTipoArchivo->tipo != $sTipo) {
            
            $aWhere = array(
                        "tipo = '{$sTipo}'",
                        "AND `ON` = 1");
            $this->_oTipoArchivo = SysTiposArchivos::find($aWhere);
        }
        
        return $this->_oTipoArchivo;
    }
    
    /**
     * 
     * @param int $nIdTipo
     * @param int $nIdEntidad
     * @return OrgArchivos
     */
    private function getArchivo($nIdTipo, $nIdEntidad)
    {
        if($this->_oArchivo == null || $this->_oArchivo == false || $this->_oArchivo->idTipo != $nIdTipo || $this->_oArchivo->idRegistro != $nIdEntidad) {
            
            $aWhere = array(
                        "idTipo = {$nIdTipo}",
                        "AND idRegistro = {$nIdEntidad}",
                        "AND `ON` = 1");
            $this->_oArchivo = OrgArchivos::find($aWhere);
        }
        
        return $this->_oArchivo;
    }
    
    /**
     * 
     * @param string $sPathArchivo
     * @return string
     */
    private function getExtArchivo($sPathArchivo)
    {
        $sExt = pathinfo($sPathArchivo, PATHINFO_EXTENSION);
        
        return $sExt;
    }
    
    /**
     * Guarda el registro de un archivo existente en la tabla de
     * org_archivos
     * 
     * @param string $sTipo
     * @param int $nIdEntidad
     * @param string $sPathArchivo
     * @param int $nSize
     * @return bool
     */
    private function guardarRegistroArchivo($sTipo, $nIdEntidad, $sPathArchivo, $nSize)
    {
        if(!file_exists($sPathArchivo)) {
            return false;
        }
        
        $oTipoArchivo = $this->getTipoArchivo($sTipo);
        $sExt = $this->getExtArchivo($sPathArchivo);
        
        $aDatos = array(
            'idTipo' => $oTipoArchivo->ID,
            'idRegistro' => $nIdEntidad,
            'ruta' => $sPathArchivo,
            'ext' => $sExt,
            'size' => $nSize);
        
        $oArchivo = OrgArchivos::create($aDatos);
        
        if($oArchivo == false) {
            return false;
        }
        
        return true;
    }
    
    public function imagenBase64($imagen)
    {
        $base64 = '';
        
        if(file_exists($imagen))
        {
            $type = pathinfo($imagen, PATHINFO_EXTENSION);
            $image = file_get_contents($imagen);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($image);
        }
        
        return $base64;
    }
}
