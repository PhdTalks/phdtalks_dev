<?php
/**
 * Clase ValidarDatos
 * La clase contiene fnciones utilizadas va ra la validación de diversos tipos
 * de datos
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package Utilerias\ValidarDatos
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */
class ValidarDatos {
    
    public static function esCorreo($sDato)
    {
        return preg_match('/^[a-z|.|_|-|0-9]{1,}[@]{1}[a-z|.|_|-|0-9]{1,}[.]{1}[a-z]{1,}$/', $sDato);
    }

    public static function esEntero($sDato)
    {
        $bResult = preg_match('/^[\d]+$/', $sDato);

        return $bResult;
    }
    
    public static function esFecha($sDato, $sFormato = 'Y-m-d')
    {
        if(strlen($sDato) >= 6 && strlen($sFormato) == 10){

            // find separator. Remove all other characters from $sFormato
            $separator_only = str_replace(array('m','d','y'),'', $sFormato);
            $separator = $separator_only[0]; // separator is first character

            if($separator && strlen($separator_only) == 2){
                // make regex
                $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $sFormato);
                $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
                $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp);
                $regexp = str_replace($separator, "\\" . $separator, $regexp);

                if($regexp != $sDato && preg_match('/'.$regexp.'\z/', $sDato)){

                    // check date
                    $arr=explode($separator,$sDato);
                    $day=$arr[0];
                    $month=$arr[1];
                    $year=$arr[2];
                    if(@checkdate($month, $day, $year))
                        return true;
                }
            }
        }
        return false;
    }
    
    public static function esHora($sDato)
    {
        $aHora = explode(':', $sDato);
        
        if(count($aHora) > 3 || count($aHora) < 2) {
            return false;
        }
        
        if($aHora[0] > 24 || $aHora[0] < 0) {
            return false;
        }
        
        if($aHora[1] > 60 || $aHora[1] < 0) {
            return false;
        }
        
        if(isset($aHora[2]) && ($aHora[2] > 60 || $aHora[2] < 0)) {
            return false;
        }
        
        return true;
    }
    
    public static function esId($sDato)
    {
        $bResult = false;
        if(ValidarDatos::esEntero($sDato) && $sDato > 0){
            $bResult = true;
        }
        
        return $bResult;
    }
    
    public static function esNull($sDato)
    {
        $bResutl = empty($sDato);
        
        return $bResutl;
    }
    
    public static function esNumero($sDato)
    {
        $bResutl = is_numeric($sDato);
        
        return $bResutl;
    }
    
    public static function esRFC($sDato)
    {
        return preg_match('/^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9,A-Z]$/', $sDato);
    }
    
    public static function esString($sDato, $nMaxLongitud = null)
    {
        $bResult = $sDato && is_string($sDato) && !is_numeric($sDato);

        if($bResult && self::esEntero($nMaxLongitud))
        {
            $bResult = strlen($sDato) <= $nMaxLongitud;
        }

        return $bResult;
    }
    
    public static function esTelefono($sDato)
    {
        //$bResult = preg_match('/^\({0,1}([0-9||-]{0,}[ ]{0,1})+\){0,1}(ext){0,1}(EXT){0,1}([.]{0,1}[ ]{0,1}[0-9]{0,5})$/', $sDato);
        //$bResult = preg_match('/^([0-9||-]{0,}[ ]{0,1})+(ext){0,1}(EXT){0,1}([.]{0,1}[ ]{0,1}[0-9]{0,5})$/', $sDato); // original
        
        //return $bResult;
        
        if(strlen($sDato) > 20)
            return false;
        
        $numeros = filter_var($sDato, FILTER_SANITIZE_NUMBER_INT); 
        return (strlen($numeros) >= 8);
    }
    
    public static function campoObligatorio($campo)
    {
        return $campo != '';
    }
}
