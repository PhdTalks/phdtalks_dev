<?php
require_once 'Session.php';

class FlashMessenger{

    static protected $_messages = array();
    protected $_namespace = 'flashMessenger';

    public function __construct(){
        $this->_messages = Session::get($this->_namespace, array());
    }

    public function addMessage($name, $message){
        $this->_messages[$name][] = $message;
        Session::add($this->_namespace, $this->_messages);
    }

    public function getMessages($name){
        if($messages = $this->_messages[$name])
        {
            $this->removeMessages($name);

            return $messages;
        }
    }

    public function removeMessages($name){
        $this->_messages[$name] = array();
        Session::add($this->_namespace, $this->_messages);
    }

    public function cleanMessages(){
        Session::add($this->_namespace, array());
    }
}