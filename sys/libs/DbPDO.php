<?php
/**
 * Clase DbPDO
 * 
 * Clase que realiza la conexion y las consultas y afectaciones a base
 * de datos hereda de la clase PDO
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package Utilerias\ValidarDatos
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */

class DbPDO extends PDO implements DbHandler {
    
    private $sUser;
    private $sPass;
    private $sAdapter;
    private $sServer;
    private $sDataBase;

    function __construct($sServer, $sDataBase, $sUser, $sPass, $sAdapter = 'mysql', $sCharset = 'utf8')
    {
        $this->sAdapter = $sAdapter;
        $this->sPass = $sPass;
        $this->sUser = $sUser;
        $this->sServer = $sServer;
        $this->sDataBase = $sDataBase;

        $eDns = "{$this->sAdapter}:host={$this->sServer};dbname={$this->sDataBase};charset={$sCharset}";

        try
        {
            parent::__construct($eDns,$this->sUser, $this->sPass);
        }
        catch(PDOException $oException)
        {
            return new Error(mysql_error($this->_connection));
        }
    }
    
    /**
     *  PDO no requiere iniciar coneccion
     * @return boolean
     */
    public function connect()
    {
        return true;
    }
    
    /**
     *  PDO no requiere terminar coneccion
     * @return boolean
     */
    public function disconnect()
    {
        return true;
    }

    public function getOneField($sField, $sSql)
    {
        $aFields = $this->getOne($sSql);
        
        if($aFields) {
            
            return $aFields[$sField];
        }
        
        return $aFields[$sField];
    }
  
    /**
     * Se obtiene un arreglo asociativo de la primer fila encontrada en query recibido
     *
     * @param string $sSql
     * @return array|boolean
     */
    function getOne($sSql)
    {
      $oStatement = $this->execute($sSql);
      if($oStatement)
      {
        $aResult = $oStatement->fetch(PDO::FETCH_ASSOC);

        return $aResult;
      }

      return false;
    }
  
    /**
     * Se obtiene un arreglo asociativo de la primer fila encontrada en query recibido
     *
     * @param string $sSql
     * @return stdClass|boolean
     */
    function getOneObject($sSql)
    {
      $oStatement = $this->execute($sSql);
      if($oStatement)
      {
        $aResult = $oStatement->fetchObject();

        return $aResult;
      }

      return false;
    }

    /**
     * Se obtiene un arreglo asociativo del resutado del query recibido
     *
     * @param string $sSql
     * @return array|boolean
     */
    function getAll($sSql)
    {
      $oStatement = $this->execute($sSql);
      if($oStatement)
      {
        $aResult = $oStatement->fetchAll(PDO::FETCH_ASSOC);

        return $aResult;
      }

      return false;
    }
    
    /**
     * Se obtiene un arreglo con objetos del resutado del query recibido
     *
     * @param string $sSql
     * @return stdClass|boolean
     */
    function getAllObject($sSql)
    {
      $oStatement = $this->execute($sSql);
      if($oStatement)
      {
        $oCollection = array();
        while ($oElemento = $oStatement->fetchObject()) {
            
            $oCollection[] = $oElemento;
        }
        
        return $oCollection;
      }

      return false;
    }
  
    /**
     * Inserta un nuevo registro en la tabla recibida y utiliza un arreglo
     * referenciado con el nombre del campo y su valor
     * 
     * @param string $sTable Nombre de la tabla
     * @param array $aData Arreglo con la informacion a ser insertada (FIELD => VALOR)
     * @return int|boolean Numero de filas afectadas.
     */
    function insert($sTable, $aData)
    {
        $aFields = array();
        $aValues = array();
        $sTable = trim($this->doQuote($sTable), "'");

        foreach($aData as $sField => $sValue)
        {
            $sField = trim($this->doQuote($sField), "'");
            $aFields[] = "`{$sField}`";

            //Para los valores nulos
            if($sValue === 'NULL')
            {
                $aValues[] = 'NULL';
                continue;
            }

            $aValues[] = $this->doQuote($sValue);
        }

        $sFields = implode(',', $aFields);
        $sValues = implode(',', $aValues);

        switch($this->sAdapter)
        {
            case 'mysql':
                  $sSql = "INSERT INTO {$sTable} ({$sFields}) VALUES ($sValues)";
                  break;
        }

        $oStatement = $this->execute($sSql);

        $nId = $this->lastInsertId();

        return $nId;
    }
    
    /**
     * Elimina el registro recibido de la tabla
     * 
     * @param string $sTable
     * @param string $sPrimaryKey
     * @param string $sKey
     * @return boolean
     */
    public function delete($sTable, $sPrimaryKey, $sKey)
    {
        switch($this->sAdapter)
        {
            case 'mysql':
                  $sSql = "DELETE FROM {$sTable} WHERE {$sPrimaryKey} = {$this->doQuote($sKey)}";
                  break;
        }

        $oStatement = $this->execute($sSql);
        if($oStatement)
        {
          return true;
        }

        return false;
    }

    /**
     * Actualizar el registro recibido con la información del array
     * 
     * @param string $sTable
     * @param string $aData
     * @param string $sPrimaryKey
     * @param string $sKey
     * @return int
     */
    public function update($sTable, $aData, $sPrimaryKey, $sKey)
    {
        $aFields = array();
        
        $sTable = trim($this->doQuote($sTable), "'");
        $sPrimaryKey = trim($this->doQuote($sPrimaryKey), "'");
        $sKey = trim($this->doQuote($sKey), "'");

        foreach($aData as $sField => $sValue)
        {
            $sField = trim($this->doQuote($sField), "'");
            $sField = "`{$sField}`";

            //Para los valores nulos
            if($sValue === "NULL")
            {
                $aFields[] = "$sField = NULL";
                continue;
            }

            $aFields[] = $sField . ' = '. $this->doQuote($sValue);
        }

        $sFields = implode(',', $aFields);

        switch($this->sAdapter)
        {
            case 'mysql':
                  $sSql = "UPDATE {$sTable} SET {$sFields} WHERE {$sPrimaryKey} = {$sKey}";
                  break;
        }

        $oStatement = $this->execute($sSql);
        
        return $sKey;
    }
    
    /**
     * Regresa un arreglo con la información del registro encontrado
     * 
     * @param string $sTable
     * @param array $aColumnas
     * @param array $aFiltros {{'Columna', 'operador', 'valor', 'bComillas(1,0)', 'AND u OR opcional'}}
     * @return array|bool
     */
    public function find($sTable, $aColumnas = array('*'), $aFiltros = array())
    {
        $sSql = $this->createSqlFind($sTable, $aColumnas, $aFiltros);
        
        return $this->getOne($sSql);
    }
    
    /**
     * Regresa un arreglo con la información del registro encontrado
     * 
     * @param string $sTable
     * @param array $aColumnas
     * @param array $aFiltros {{'Columna', 'operador', 'valor', 'bComillas(1,0)', 'AND u OR opcional'}}
     * @return array|bool
     */
    public function findAll($sTable, $aColumnas = array('*'), $aFiltros = array())
    {
        $sSql = $this->createSqlFind($sTable, $aColumnas, $aFiltros);
        
        return $this->getAll($sSql);
    }
    
    /**
     * Obtiene un resource del query enviado o false en caso de error
     * 
     * @param string $sSql
     * @return PDOStatement|boolean
     */
    public function execute($sSql)
    {
      $oStatement = $this->prepare($sSql);
      if($oStatement->execute())
      {
          return $oStatement;
      }

      return false;
    }
  
    /**
     * Escapa los caracteres especiales en una cadena y se retorna la cadena entre '.
     * 
     * @param string $sString Cadena a ser escapada
     * @return string Cadena escapada.
     */
    public function doQuote($sString)
    {
      return $this->quote($sString);
    }
    
    /**
     * Crea un query utilizando los datos recibidos y regresa la instrucción
     * necesaria segun el tipo de adaptador
     * 
     * @param string $sTable
     * @param array  $aColumnas
     * @param array  $aFiltros
     * @return string
     */
    private function createSqlFind($sTable, $aColumnas = array('*'), $aFiltros = array())
    {
        $aWhere = array();
        $sColumnas = implode(',', $aColumnas);
        
        switch($this->sAdapter)
        {
            case 'mysql':
                foreach($aFiltros as $aUnFiltro)
                {
                    $aWhere[] = $aUnFiltro;

                    /* DESPUES NOS PONDREMOS DE ACUERDO LUIS Y YO COMO ESTRUCTURAR ESTO
                       PARA QUE LOS FILTROS SE AGREGUEN MEJOR ORDENADOS RECIBIENDO UNA ESTRUCTURA
                       TIPO $campo $operador $valor $condicion O CHECAMOS COMO LO HACE ALGUN FRAMEWORK
                       POR EL MOMENTO SERA UN ARREGLO SIMPLE

                    if(!is_array($aUnFiltro))
                    {
                        $aWhere[] = $aUnFiltro;
                        continue;
                    }

                    if(count($aUnFiltro) == 3)
                    {
                        $aWhere[] = "$aUnFiltro[0] {$aUnFiltro[1]} '{$aUnFiltro[2]}'";
                        continue;
                    }

                    if(count($aUnFiltro) == 4)
                    {
                        if($aUnFiltro[3] == 1)
                        {
                            $aWhere[] = "$aUnFiltro[0] {$aUnFiltro[1]} '{$aUnFiltro[2]}'";
                        }
                        else
                        {
                            $aWhere[] = "$aUnFiltro[0] {$aUnFiltro[1]} {$aUnFiltro[2]}";
                        }

                        continue;
                    }

                    if(count($aUnFiltro) == 5)
                    {
                        if($aUnFiltro[3] == 1)
                        {
                            $aWhere[] = "{$aUnFiltro[4]} $aUnFiltro[0] {$aUnFiltro[1]} '{$aUnFiltro[2]}'";
                        }
                        else
                        {
                            $aWhere[] = "{$aUnFiltro[4]} $aUnFiltro[0] {$aUnFiltro[1]} {$aUnFiltro[2]}";
                        }

                        continue;
                    }
                    */
                }

                $sWhere = implode(' ', $aWhere);

                $sSql = "SELECT {$sColumnas} FROM {$sTable} WHERE {$sWhere}";
                break;
        }

        return $sSql;
    }
}